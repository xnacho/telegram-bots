<?php

/**
 * 
 * 
 */

include_once '../LogicaGeneral.php';

/*******************************/
/* Variables y Configuraciones */
/*-----------------------------*/

$logica = new LogicaGeneral('Gálvez', 'degvz', 'Santa Fe', 'desfe');
$logica->cargarTgBotsApi('100975145:AAGZOK3NzkkHwfxS5FCqCkIcaUGdWs4RYr0');
$logica->cargarBDlocal('GalvanicaBot', 'GalvanicaBot_contacto');


$deGvz = array(
    array('04:45',	'Ciudad de Gálvez',	0),
    array('05:59',	'Ciudad de Gálvez',	0),
    array('06:45',	'Ciudad de Gálvez',	1),
    array('09:30',	'Ciudad de Gálvez',	3),
    array('11:35',	'Espiga de Oro',	0),
    array('13:15',	'Ciudad de Gálvez',	0),
    array('14:40',	'Ciudad de Gálvez',	0),
    array('17:05',	'Espiga de Oro',	0),
    array('19:15',	'Ciudad de Gálvez',	2),
    array('21:40',	'Espiga de Oro',	0),
    array('23:25',	'Espiga de Oro',	0),
);

$deSfe = array(
    array('05:50',	'Espiga De Oro',	0),
    array('06:50',	'Espiga De Oro',	0),
    array('09:50',	'Ciudad de Gálvez',	0),
    array('10:55',	'Ciudad de Gálvez',	0),
    array('12:30',	'Ciudad de Gálvez',	1),
    array('14:30',	'Ciudad de Gálvez',	3),
    array('16:00',	'Ciudad de Gálvez',	0),
    array('16:55',	'Espiga de Oro',	0),
    array('18:30',	'Ciudad de Gálvez',	0),
    array('20:00',	'Espiga de Oro',	0),
    array('20:45',	'Ciudad de Gálvez',	2),
);

$observaciones = array(
    array(
        'dias'  => array(),
        'feriados'  => false,
        'desc'  => 'Por Ruta 11'),
    array(
        'dias'  => array('Sun'),
        'feriados'  => false,
        'desc'  => 'Por Autopista (Lun-Sáb)'),
    array(
        'dias'  => array('Sat'),
        'feriados'  => false,
        'desc'  => 'Por Autopista (Dom-Vie)'),
    array(
        'dias'  => array('Sat','Sun'),
        'feriados'  => true,
        'desc'  => 'Por Ruta 19 (Lun-Vie, días hábiles)'),
);

$logica->cargarHorarios($deGvz, $deSfe, $observaciones);


$precios = array(
    array('Intermedias',	'64,00'),
    array('    Directo',	'76,00'),
);

$logica->cargarPrecios($precios);

$logica->procesarPost();

?>
