<?php
function cuandoPasa($peticion = '', $recurso = '') {

	$c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($c, CURLOPT_CUSTOMREQUEST, 'HEAD');
    curl_setopt($c, CURLOPT_HEADER, true);
    curl_setopt($c, CURLOPT_NOBODY, true);
    curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:26.0) Gecko/20100101 Firefox/25.0');
    curl_setopt($c, CURLOPT_URL, 'http://cuandopasa.efibus.com.ar/');
    $res = curl_exec($c);
	
	preg_match('/^Set-Cookie:\s*([^;]*)/mi', $res, $m);
    $cookie = $m[1];

	$encabezados = array(
        'Content-Type: application/json; charset=utf-8',
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language: es-ar,es;q=0.8,en-us;q=0.5,en;q=0.3',
        'Accept-Encoding: gzip, deflate',
        'X-Requested-With: XMLHttpRequest',
        'Referer: http://cuandopasa.efibus.com.ar/',
        'Connection: keep-alive',
        'Host: cuandopasa.efibus.com.ar',
        'Pragma: no-cache',
        'Cache-Control: no-cache',
    );

	$c = curl_init();
    curl_setopt($c, CURLOPT_POST, true);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($c, CURLOPT_HEADER, false);
    curl_setopt($c, CURLOPT_HTTPHEADER, $encabezados);
    curl_setopt($c, CURLOPT_COOKIE, $cookie);
    curl_setopt($c, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; rv:25.0) Gecko/20100101 Firefox/25.0');
    curl_setopt($c, CURLOPT_POSTFIELDS, json_encode($peticion));
    curl_setopt($c, CURLOPT_URL, 'http://cuandopasa.efibus.com.ar/default.aspx/RecuperarDatosDeParada' . $recurso);
	
    curl_setopt($c, CURLOPT_TIMEOUT, 3);
    $res = json_decode(curl_exec($c));

	// proceso el resultado para mostrarlo al usuario
	if ($res && property_exists($res, 'd')) {
		$respuesta = embellecerResultado($res->d);
    }
	else {
        $respuesta .= 'La consulta no generó respuesta. Compruebe el número de parada.';
    }
	
	return $respuesta;
}

function embellecerResultado($resultados)
{
	$infoParadas = array();
	
	foreach ($resultados as $dato){
		$linea = trim(strtok($dato->datosMostrar, '|'));
		$texto = trim(strtok('|'));
		
		$infoParadas[$linea][] = $texto;
	}
	
	return embellecerParadas($infoParadas);
}

function embellecerParadas($infoParadas)
{
	$salidas = '';
	
	ksort($infoParadas);
	
	foreach ($infoParadas as $linea=>$horarios) {
		if (is_string($linea)) {
			continue;
		}

		if ($linea < 10) {
			$linea = ' '.$linea;
		}
		
		foreach ($horarios as $horario){
			if (strlen($horario) > 0) {
				$salidas .= "`$linea` *|* $horario \n";
			}
			else {
				$salidas .= "*$linea*";
			}
		}

		$salidas .= "\n";
	}
	
	return $salidas;
}

?>