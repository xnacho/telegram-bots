<?php

class TgBotsApi
{
	var $token;
	
	function TgBotsApi($token)
	{
		$this->token = $token;
		date_default_timezone_set('America/Argentina/Cordoba');
	}
	
	/**
	 * 
	 * 
	 */
	function getUpdates()
	{
		$url = "https://api.telegram.org/bot$this->token/getUpdates";

		$data = array(
			'offset'    => 80,
			'limit'     => 10,
			'timeout'   => 0,
		);
		
		return $this->enviarPost($url, $data);
	}
	
	/**
	 * Envía la url para establecer el webhook del bot instanciado.
	 * 
	 * @createDate 2016-04-27 16:54
	 * @lastmodifiedDate 2016-04-27 20:6 
	 * @lastmodifiedBy Nacho S.
	 * 
	 * @author Nacho S.
	 * @param  string $url servidor que recibirá los mensajes del bot
	 * @return string resultado de la llamada POST a la api de Telegram
	 */
	function setWebhook($webhook = '')
	{
		$url = "https://api.telegram.org/bot$this->token/setWebhook";
		
		// Compruebo si debo eliminar el webhook
		if ('delete' == $webhook) {
			return $this->enviarGet($url);
		}

		// Cargo la URL del webhook que deseo establecer
		$data = array(
				'url'    => $webhook,
			);
		
		return $this->enviarPost($url, $data);
	}
	
	/**
	 * 
	 * 
	 */
	function sendMessage($chatID,
						 $text,
						 $parseMode = null,
						 $replyMarkup = null,
						 $disablePreview = null,
						 $replyTo = null)
	{
		$url = "https://api.telegram.org/bot$this->token/sendMessage";
		$data = array(
			'chat_id'					=> $chatID,
			'text'						=> $text,
			'parse_mode'				=> $parseMode,
			'disable_web_page_preview'	=> $disablePreview,
			'reply_to_message_id'		=> $replyTo,
			'reply_markup'				=> $replyMarkup,
		);
		
		return $this->enviarPost($url, $data);
	}
	
	/**
	 * 
	 * 
	 */
	private function enviarPost($url, $data)
	{
		// use key 'http' even if you send the request to https://...
		$options = array(
			'http'  => array(
				'header'    => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'    => 'POST',
				'content'   => http_build_query($data),
			),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		
		return $result;
	}

	/**
	 * Envía un GET básico a la URL solicitada
	 * @createDate 2016-04-27 17:48
	 * @lastmodifiedDate 2016-04-27 17:48
	 * @lastmodifiedBy Nacho S.
	 * 
	 * @author Nacho S.
	 * @param  string $url del recurso al que deseo enviar un GET
	 * @return string resultado de la solicitud
	 */
	private function enviarGet($url)
	{
		// use key 'http' even if you send the request to https://...
		$options = array(
			'http'  => array(
				'header'    => "Content-type: application/x-www-form-urlencoded\r\n",
				'method'    => 'GET',
			),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		
		return $result;
	}
}

?>