<?php

/**
 * 
 * 
 */

include_once 'cuandopasa.php';
include_once '../TgBotsApi.php';
include_once '../BDlocal.php';

/*******************************/
/* Variables y Configuraciones */
/*-----------------------------*/
$cuandoPasaSantaFe_bot = new TgBotsApi('203850065:AAGVR88Im79irkytg1sHZiQAZZSKn4rycw0');

$bd = new BDlocal('CuandoPasaSantaFe_bot', 'CuandoPasaSantaFe_bot_contacto');


/* Compruebo para setear el webhook */
if ( isset($_GET['webhook']) ) {
	// Invoco al método para establecer el webhook
    echo $cuandoPasaSantaFe_bot->setWebhook($_GET['webhook']);
	
	// Termino la ejecución del script
	exit;
}



/************/
/* Mensajes */
/*----------*/

$descripcionComandos =
	"*Opciones disponibles:*\n".
	"/<nro. parada> - para conocer el tiempo estimado de arribo de todas las líneas de esa parada.\n".
	"*Ejemplo:* /27231\n";


$mensajes['bienvenida']	= 
	"¡Hola!\n\n".

	"En todo momento, te puedo informar sobre el tiempo de arribo de las líneas ".
	"de colectivos urbanos de Santa Fe \xE2\x8C\x9A \xF0\x9F\x98\x89\n\n".

	$descripcionComandos;

$mensajes['ayuda'] = 
	"Podrás obtener los tiempos estimados de arribo de los colectivos a la parada solicitada.\n\n".
	"*Ciudad:* Santa Fe, AR\n\n".$descripcionComandos;

$mensajes['contactar'] = 
	"Podés enviarme una consulta o sugerencia escribiendola en un mensaje ".
	"único.\n\n".
	"Usa /cancelar para volver el tiempo atrás y hacer como si nada hubiese pasado.";

$mensajes['contactar_error'] = 
	"No pude leer tu mensaje, ¡probá de nuevo!\n".
	"Por las dudas, no me mandes stickers...\n\n".
	"Usa /cancelar para anular esta acción.";

$mensajes['contactar_gracias'] = 
	"¡Gracias por escribirme! ☺";

$mensajes['cancelar'] = 
	"¡Listo! Como si nada hubiese pasado...";

$mensajes['configuraciones'] = 
	"¡Por el momento, no hay nada para configurar!\n\n".
	"Para más ayuda, utiliza el comando /help.";

$mensajes['default'] = 
	"*Algo salió mal...*\n\n".
	"Te envío la lista de comandos para que veas lo que podés hacer.\n\n".$descripcionComandos;



/******************/
/* Lógica general */
/*----------------*/

// decodifico el mensaje recibido desde Telegram
$request = file_get_contents('php://input');
$input = json_decode($request);

/* Extraigo toda la información del mensaje enviado por el usuario */
$mensaje = utf8_decode($input->message->text);
$comando = strtok($mensaje, ' ');
$argumento = strtok(' ');

$usuario = $input->message->from;


/* Según el mensaje recibido, proceso la información para enviar la
 * respuesta */
switch (strtolower($comando)) {
	case fnmatch("/[0-9][0-9][0-9][0-9][0-9]",$comando):
		$bd->registrarConsulta($usuario);
		$nroParada = strtok($comando,'/');
		
		$peticion = array(
			"identificadorParada" => $nroParada,
		);

		$respuesta = "Próximos arribos en la parada *$nroParada*:\n\n";
		$respuesta .= cuandoPasa($peticion);
		break;
	
	case '/contactar':
		$bd->solicitarContacto($usuario);
		$respuesta = $mensajes['contactar'];
		$teclado = json_encode($replyKeyboardHide);
		break;

	case '/cancelar':
		if ($bd->contactoSolicitado($usuario)) {
			$respuesta = $mensajes['cancelar'];
		}
		$bd->cancelarContacto($usuario);
		break;

	case '/ayuda':
	case '/help':
		$bd->cancelarContacto($usuario);
		$respuesta = $mensajes['ayuda'];
		break;

	case '/start':
		$bd->inicializarUsuario($usuario);
		$respuesta = $mensajes['bienvenida'];
		break;
		
	case '/settings':
		$bd->cancelarContacto($usuario);
		$respuesta = $mensajes['configuraciones'];
		break;

	default:
		if ($bd->contactoSolicitado($usuario)) {
			if ($bd->guardarMensajeContacto($usuario, $mensaje)) {
				/* Configuro los parámetros para notificar sobre el 
				 * contacto realizado */
				$first_name = utf8_decode($usuario->first_name);
				$last_name = utf8_decode($usuario->last_name);
				$username = utf8_decode($usuario->username);

				$contacto =
					"Contacto de: $first_name $last_name (@$username)\n\n".
					"Mensaje: $mensaje";

				// Envío el mensaje de contacto al creador del bot
				$botsApi->sendMessage(4838085, $contacto);

				// Configuro la respuesta para el usuario
				$respuesta = $mensajes['contactar_gracias'];
			}
			else {
				$respuesta = $mensajes['contactar_error'];
				//$teclado = json_encode($replyKeyboardHide);
			}
		}
		else {
			$respuesta = $mensajes['default'];
		}
}

// Envío la respuesta correspondiente al chat de origen
$cuandoPasaSantaFe_bot->sendMessage($usuario->id,
									$respuesta,
									'Markdown');

?>
