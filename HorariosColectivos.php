<?php


/******************************************************************************/

class HorariosColectivos
{
	function HorariosColectivos($deCiudadA, $deCiudadB, $observaciones)
	{
		date_default_timezone_set('America/Argentina/Cordoba');
		$this->fechaActual = date('Y/m/d');
		
		$this->maxCantHorarios = 4;
		
		$this->deCiudadA = $deCiudadA;
		$this->deCiudadB = $deCiudadB;
		
		$this->observaciones = $observaciones;
		
		// Cargo los 24 íconos de relojes disponibles.
		$this->relojes = array(
			"\xF0\x9F\x95\x90",
			"\xF0\x9F\x95\x91",
			"\xF0\x9F\x95\x92",
			"\xF0\x9F\x95\x93",
			"\xF0\x9F\x95\x94",
			"\xF0\x9F\x95\x95",
			"\xF0\x9F\x95\x96",
			"\xF0\x9F\x95\x97",
			"\xF0\x9F\x95\x98",
			"\xF0\x9F\x95\x99",
			"\xF0\x9F\x95\x9A",
			"\xF0\x9F\x95\x9B",
			"\xF0\x9F\x95\x9C",
			"\xF0\x9F\x95\x9D",
			"\xF0\x9F\x95\x9E",
			"\xF0\x9F\x95\x9F",
			"\xF0\x9F\x95\xA0",
			"\xF0\x9F\x95\xA1",
			"\xF0\x9F\x95\xA2",
			"\xF0\x9F\x95\xA3",
			"\xF0\x9F\x95\xA4",
			"\xF0\x9F\x95\xA5",
			"\xF0\x9F\x95\xA6",
			"\xF0\x9F\x95\xA7",
		);
	}
	
	/**
	 * 
	 * 
	 */
	function getHorariosA($nombreOrigen, $hora = NULL)
	{
		return $this->getHorarios($this->deCiudadA, $nombreOrigen, $hora);
	}
	
	/**
	 * 
	 * 
	 */
	function getHorariosB($nombreOrigen, $hora = NULL)
	{
		return $this->getHorarios($this->deCiudadB, $nombreOrigen, $hora);
	}
	
	/**
	 * 
	 * 
	 */
	private function getHorarios($origen, $nombreOrigen, $hora)
	{
		$comprobarDiasEspeciales = true;
		
		// Elijo uno de los 24 íconos de relojes de acuerdo a la hora actual
		$h = idate("h");
		$m = idate("i");
		
		// Elijo el reloj de la hora exacta
		$r = $h-1;
		if ($m > 29) {
			// Elijo el reloj que marca las y media
			$r += 12;
		}

		$reloj = $this->relojes[$r]; 
		
		// Compruebo si se ha pasado un parámetro válido
		if (is_numeric($hora)
			and intval($hora) >= 0
			and intval($hora) < 24) {
				/* resto un segundo al resultado para que incluya las salidas
				 * en esa hora incluída. */
				$horaActual = $this->horaEnSegundos($hora) - 1;
				
				// Deshabilito el filtrado de horarios para que muestre todos
				$comprobarDiasEspeciales = false;
			
				// Formateo la hora para que se muestre HH:MM
				$h = str_pad($hora, 2, "0", STR_PAD_LEFT) . ':00';
				$proximosHorarios = "$reloj Salidas ".
					"*desde $nombreOrigen* (_a partir de las ${h}_):\n\n";
		}
		else {
			$horaActual = $this->horaEnSegundos(date('H:i'));
			$proximosHorarios = "$reloj Próximas salidas ".
				"*desde $nombreOrigen*:\n\n";
		}   


		
		// Inicializo el contador de horarios generados para mostrar
		$cantHorarios = 0;

		/* Recorro los horarios disponibles para seleccionar aquellos
		 * que serán devueltos */
		foreach ($origen as $horario) {
			if ($comprobarDiasEspeciales) {
				if ( ! $this->horarioValido($horario, $this->fechaActual))
					continue;
			}
			
			$salida = $this->horaEnSegundos($horario[0]);

			if ($horaActual < $salida) {
				$proximosHorarios .= $this->imprimirHorario($horario);
				
				if ($this->maxCantHorarios == ++$cantHorarios) {
					break;
				}
			}
		}
		
		/* Al finalizar el recorrido del vector, compruebo si se
		 * pudo alcanzar la cantidad de horarios a mostrar establecidos.
		 * De lo contrario, recorro el vector desde el inicio */
		if ($cantHorarios < $this->maxCantHorarios) {
			$fecha = date('Y/m/d', strtotime("$this->fechaActual + 1 days"));
			foreach ($origen as $horario) {
				if ($comprobarDiasEspeciales) {
					if ( ! $this->horarioValido($horario, $fecha))
						continue;
				}

				$proximosHorarios .= $this->imprimirHorario($horario);

				if ($this->maxCantHorarios == ++$cantHorarios) {
					break;
				}
			}
		}
		
		//Agrego los íconos finales (tipo corriendo al cole)
		$proximosHorarios .= "\xF0\x9F\x9A\x8C \xF0\x9F\x92\xA8 \xF0\x9F\x8F\x83";
		
		return $proximosHorarios;
	}
	
	/**
	 * Comprueba si un horario dado es válido para la fecha
	 * pasada como parámetro.
	 * 
	 * @author Nacho S.
	 * @createDate 2016-02-13
	 * @lastmodifiedDate 2016-02-14
	 * 
	 * @param  array   $horario información del horario a evaluar
	 * @param  string  $fecha   fecha en formato Y/m/d
	 * @return boolean true en caso de que sea válido
	 *                          false en caso contrario.
	 */
	private function horarioValido($horario, $fecha)
	{
		$diaActual = date( 'D', strtotime($fecha) );
		// Compruebo que salga en el día actual
		foreach ($this->observaciones[$horario[2]]['dias']
				 as $dias) {
			if (0 == strcmp($diaActual, $dias)) {
				return false;
			}
		}

		// Compruebo si sale los días feriados
		if ($this->observaciones[$horario[2]]['feriados']) {
			if ($this->esFeriado($fecha))
				return false;
		}
		
		return true;
	}

	/**
	 * Comprueba si el día actual es feriado.
	 * 
	 * @createDate 09/25/2015
	 * @lastmodifiedBy Nacho S.
	 * @lastmodifiedDate 2016-02-13 16:17:27 UTC-3
	 * 
	 * @return	boolean	true	si es feriado
	 * 					false	en caso contrario
	 */
	function esFeriado($fecha)
	{
		$feriados = array(
			'2016/01/01',
			'2016/02/08',
			'2016/02/09',
			'2016/03/24',
			'2016/03/25',
			'2016/04/02',
			'2016/05/01',
			'2016/05/25',
			'2016/06/20',
			'2016/07/08',
			'2016/07/09',
			'2016/08/15',
			'2016/10/04',
			'2016/10/10',
			'2016/10/12',
			'2016/11/28',
			'2016/12/08',
			'2016/12/09',
			'2016/12/25',
		);

		foreach ($feriados as $feriado) {
			if (0 == strcmp($fecha, $feriado))
				return true;
		}

		return false;
	}
	
	/**
	 * Formatea cada salida
	 * 
	 * @createDate 09/25/2015
	 * @lastmodifiedBy Nacho S.
	 * @lastmodifiedDate 2016-02-13 16:17:27 UTC-3
	 * 
	 * @param  array	$horario	conteniendo horario, empresa y salidas.
	 * @return string				horario formateado.
	 */
	private function imprimirHorario($horario)
	{
		return ("*${horario[0]}* | ${horario[1]} _--".
				$this->observaciones[$horario[2]]['desc']."--_\n\n");
	}
	
	/**
	 * Convierte un string representando una hora (HH:MM:SS)
	 * en la cantidad de segundos que representa.
	 * 
	 * @createDate 09/25/2015
	 * @lastmodifiedBy Nacho S.
	 * @lastmodifiedDate 2015-09-25 18:59:53 UTC-3
	 * 
	 * @param  string	$hora	en formato HH:MM:SS (se pueden omitir de
	 * 							derecha a izquierda).
	 * @return integer			cantidad de segundos que representa la hora.
	 */
	private function horaEnSegundos($hora)
	{
		$horas = intval(strtok($hora, ':'));
		
		$mins = intval(strtok(':'));
		
		if ($horas > 0) {
			$mins += ($horas*3600);
		}

		return $mins;
	}
}

?>