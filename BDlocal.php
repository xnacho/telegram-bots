<?php

class BDlocal
{
	var $tablaUsuario;
	var $tablaContacto;
	
	function BDlocal($tablaUsuario, $tablaContacto)
	{
		$this->tablaUsuario = $tablaUsuario;
		$this->tablaContacto = $tablaContacto;
	}

	/**
	 * 
	 */
	function inicializarUsuario($usuario, $solicitarContacto = 0)
	{
		$conn = $this->conectarBD();
		
		// Check connection
		if ( ! $conn) {
		    return false;
		}
	
		// alamceno la fecha actual formateada para guardar en la BD
		$fecha = "'".date('Y-m-d H:i:s')."'";
	
		// Almaceno las propiedades del usuario formateadas para guardar en la BD
		$first_name = "'".utf8_decode($usuario->first_name)."'";
		$last_name = "'".utf8_decode($usuario->last_name)."'";
		$username = "'".utf8_decode($usuario->username)."'";
	
	
		// Compruebo si el usuario es nuevo o no
	    $sql = "SELECT 1 FROM $this->tablaUsuario WHERE id = $usuario->id";
	    $result = mysqli_query($conn, $sql);
	
		// si hay un resultado, es porq existe en la tabla
	    if ($row = mysqli_fetch_assoc($result)) {
			return true;
	    }
	
		
		// Inserto un usuario nuevo
		$sql =
		    "INSERT INTO $this->tablaUsuario ".
		    "VALUES ($usuario->id, ".
		    		"$first_name, $last_name, $username, $fecha, $fecha, 1, $solicitarContacto)";
	
		return $this->consultarBD($conn, $sql);
	}
	
	/**
	 * Guarda un registro en la BD sobre el usuario, almacenando la primera 
	 * y última vez que realizó alguna consulta
	 */
	function registrarConsulta($usuario)
	{
		$conn = $this->conectarBD();
		
		// Check connection
		if ( ! $conn) {
		    return false;
		}
	
		// alamceno la fecha actual formateada para guardar en la BD
		$fecha = "'".date('Y-m-d H:i:s')."'";
	
		// Almaceno las propiedades del usuario formateadas para guardar en la BD
		$first_name = "'".utf8_decode($usuario->first_name)."'";
		$last_name = "'".utf8_decode($usuario->last_name)."'";
		$username = "'".utf8_decode($usuario->username)."'";
	
	
		// Compruebo si el usuario es nuevo o no
	    $sql = "SELECT 1 FROM $this->tablaUsuario WHERE id = $usuario->id";
	    $result = mysqli_query($conn, $sql);
	
		// si hay un resultado, es porq existe en la tabla
	    if ($row = mysqli_fetch_assoc($result)) {
	        // actualizo la cantidad de consultas y la fecha
			$sql =
			    "UPDATE $this->tablaUsuario ".
			    "SET first_name = $first_name, last_name = $last_name, ".
			    "username = $username, ultima_consulta = $fecha, ".
			    "cant_consultas = cant_consultas + 1, contactar = 0 ".
			    "WHERE id = $usuario->id";
	
			return $this->consultarBD($conn, $sql);
	    }
	    
	    return $this->inicializarUsuario($usuario);
	}
	
	
	function solicitarContacto($usuario)
	{
		$conn = $this->conectarBD();
		
		// Check connection
		if ( ! $conn) {
		    return false;
		}
	
		// Compruebo si el usuario es nuevo o no
	    $sql = "SELECT 1 FROM $this->tablaUsuario WHERE id = $usuario->id";
	    $result = mysqli_query($conn, $sql);
	
		// si hay un resultado, es porq existe en la tabla
	    if ($row = mysqli_fetch_assoc($result)) {
		    // actualizo la cantidad de consultas y la fecha
			$sql =
			    "UPDATE $this->tablaUsuario ".
			    "SET contactar = 1 ".
			    "WHERE id = $usuario->id";
	
			return $this->consultarBD($conn, $sql);
	    }
	    
	    // inicializo el usuario con una solicitud de contacto
	    return $this->inicializarUsuario($usuario, 1);
	}
	
	function contactoSolicitado($usuario)
	{
		$conn = $this->conectarBD();
		
		// Check connection
		if ( ! $conn) {
		    return false;
		}
		
		// Compruebo si el usuario es nuevo o no
	    $sql = "SELECT contactar FROM $this->tablaUsuario WHERE id = $usuario->id";
	    $result = mysqli_query($conn, $sql);
	
		// si hay un resultado, es porq existe en la tabla
	    if ($row = mysqli_fetch_assoc($result)) {
	    	return $row['contactar'];
	    }
	    
	    return false;
	}
	
	
	function cancelarContacto($usuario)
	{
		$conn = $this->conectarBD();
		
		// Check connection
		if ( ! $conn) {
		    return false;
		}
	
	    // Cancelo la solicitud para contactarse
		$sql =
		    "UPDATE $this->tablaUsuario ".
		    "SET contactar = 0 ".
		    "WHERE id = $usuario->id";
	
		return $this->consultarBD($conn, $sql);
	}
	
	function guardarMensajeContacto($usuario, $mensaje)
	{
		if (0 == strlen($mensaje)) {
			return false;
		}
		
		$conn = $this->conectarBD();
		
		// Check connection
		if ( ! $conn) {
		    return false;
		}
	
		// alamceno la fecha actual formateada para guardar en la BD
		$fecha = date('Y-m-d H:i:s');
	
		// Almaceno las propiedades del usuario formateadas para guardar en la BD
		$first_name = utf8_decode($usuario->first_name);
		$last_name = utf8_decode($usuario->last_name);
		$username = utf8_decode($usuario->username);

		// prepare and bind
		$sql =
		    "INSERT INTO $this->tablaContacto ".
		    "(fecha, user_id, first_name, last_name, username, mensaje) ".
		    "VALUES (?,?,?,?,?,?)";
		$stmt = $conn->prepare($sql);
		$stmt->bind_param("ssssss", $fecha, $usuario->id, $first_name,
									$last_name, $username, $mensaje);
		
		if ( ! $stmt->execute()) {
			return false;
		}

	    // Cancelo la solicitud para contactarse
		$sql =
		    "UPDATE $this->tablaUsuario ".
		    "SET contactar = false ".
		    "WHERE id = $usuario->id";
	
		if ( ! $this->consultarBD($conn, $sql)) {
			return false;
		}
		
		return true;
	}
	
	
	function conectarBD()
	{
		// Connect to the database
		$host   = "mysql.hostinger.com.ar";			//See Step 3 about how to get host name
		$user   = "u179786844_tgbot";       //Your Cloud 9 username
		$pass   = "mtUJa0R3XuQKsMhH";       //Remember, there is NO password!
		$db     = "u179786844_tgbot";		//Your database name you want to connect to
		$port   = 3306;                 //The port #. It is always 3306
		
	
		// Create connection
		return mysqli_connect($host, $user, $pass, $db, $port);
	}
	
	
	/**
	 * Realiza una consulta SQL a la BD y al terminar cierra la conexión.
	 * Devuelve TRUE o FALSE según el éxito de la consulta.
	 */
	function consultarBD($conn, $sql)
	{
		if (mysqli_query($conn, $sql)) {
		    mysqli_close($conn);
		    return true;
		}
	
	    mysqli_close($conn);
	    return false;
	}
}


?>