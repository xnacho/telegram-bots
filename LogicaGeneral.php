<?php
include_once 'HorariosColectivos.php';
include_once 'TgBotsApi.php';
include_once 'BDlocal.php';

class LogicaGeneral
{
	var $ciudadA;
	var $comandoA;
	var $ciudadB;
	var $comandoB;
	
	function LogicaGeneral($ciudadA, $comandoA, $ciudadB, $comandoB)
	{
		$this->ciudadA = $ciudadA;
		$this->comandoA = $comandoA;
		$this->ciudadB = $ciudadB;
		$this->comandoB = $comandoB;

		/****************************/
		/* Mensajes preconfigurados */
		/*--------------------------*/

		$descripcionComandos =
				"/$comandoA - próximos horarios que salen desde $ciudadA.\n".
				"/$comandoA <hora> - próximos horarios que salen desde $ciudadA a partir ".
				"de tal <hora> (números entre 0 y 23).\n".
				"/$comandoB - próximos horarios que salen desde $ciudadB.\n".
				"/$comandoB <hora> - próximos horarios que salen desde $ciudadB a partir ".
				"de tal <hora> (números entre 0 y 23).\n".
				"/precio - Listado de precios de los distintos tipos de pasaje.\n".
				"/contactar - escribime un mensaje para comentar o sugerir algo.\n";

		$this->mensajes['bienvenida']	= 
				"¡Hola, gracias por escribirme!\n\n".
				"Cuando lo necesites, podés consultar información sobre los horarios y precios ".
				"de los viajes a través de estos comandos:\n\n".$descripcionComandos;

		$this->mensajes['ayuda'] = 
				"Este bot te brindará los próximos horarios de partida de los ".
				"colectivos, entre $ciudadA y $ciudadB, en ambos sentidos.\n\n".$descripcionComandos;

		$this->mensajes['contactar'] = 
				"Podés enviarme una consulta o sugerencia escribiendola en un mensaje ".
				"único.\n\n".
				"Usa /cancelar para volver el tiempo atrás y hacer como si nada hubiese pasado.";

		$this->mensajes['contactar_error'] = 
				"No pude leer tu mensaje, ¡probá de nuevo!\n".
				"Por las dudas, no me mandes stickers...\n\n".
				"Usa /cancelar para anular esta acción.";

		$this->mensajes['contactar_gracias'] = 
				"¡Gracias por escribirme! ☺";

		$this->mensajes['cancelar'] = 
				"¡Listo! Como si nada hubiese pasado...";

		$this->mensajes['configuraciones'] = 
				"¡Por el momento, no hay nada para configurar!\n\n".
				"Para más ayuda, utiliza el comando /help.";

		$this->mensajes['default'] = 
				"¿Cómo? ¿Podés repetirme la consulta?\n\n".$descripcionComandos;
	}
	
	function cargarTgBotsApi($token)
	{
		$this->botsApi = new TgBotsApi($token);
	}
	
	function cargarBDlocal($tablaPrincipal, $tablaContacto)
	{
		$this->bd = new BDlocal($tablaPrincipal, $tablaContacto);
	}
	
	function cargarHorarios($deCiudadA, $deCiudadB, $observaciones)
	{
		$this->horarios = new HorariosColectivos($deCiudadA,
                                                 $deCiudadB,
                                                 $observaciones);
	}

	function cargarPrecios($precios, $urlPrecios = NULL) {
		$this->mensajes['precio'] = 
			"\xE2\x84\xB9 *Precio del pasaje* \xE2\x84\xB9\n\n";
		
		foreach ($precios as $precio) {
			$this->mensajes['precio'] .=
				"`$precio[0]: `\xF0\x9F\x92\xB2$precio[1]\n";
		}
		
		$this->mensajes['precio'] .=
			"\n\xF0\x9F\x91\x89 _Los valores pueden diferir de los anteriores._\n".
			(is_null($urlPrecios) ?
			 "_Para notificar otros precios, utilice /contactar" :
			 "_Más información: _ [Lista de precios online]($urlPrecios).");
	}
	
	function procesarPost()
	{
		// decodifico el mensaje recibido desde Telegram
		$request = file_get_contents('php://input');
		$input = json_decode($request);

		/* Extraigo toda la información del mensaje enviado por el usuario */
		$mensaje = utf8_decode($input->message->text);
		$comando = strtok($mensaje, ' ');
		$argumento = strtok(' ');

		$usuario = $input->message->from;

		// Configuro los tipos de teclado que serán enviados como respuesta
		$replyKeyboardMarkup = array(
			'keyboard'	=> array(
				array(
					"/$this->comandoA",
					"$",
					"/$this->comandoB",
				),
			),
			'resize_keyboard'	=> true,
			'one_time_keyboard'	=> false,
		);

		$replyKeyboardHide = array(
			'hide_keyboard'	=> true,
		);

		$teclado = json_encode($replyKeyboardMarkup);

		/* Según el mensaje recibido, proceso la información para enviar la
		 * respuesta */
		switch (strtolower($comando)) {
			case "/$this->comandoA":
				$this->bd->registrarConsulta($usuario);
				$respuesta = $this->horarios->getHorariosA($this->ciudadA,
                                                           $argumento);
				break;

			case "/$this->comandoB":
				$this->bd->registrarConsulta($usuario);
				$respuesta = $this->horarios->getHorariosB($this->ciudadB,
                                                           $argumento);
				break;

			case '/precio':
			case '$':
				$this->bd->cancelarContacto($usuario);
				$respuesta = $this->mensajes['precio'];
				break;

			case '/help':
				$this->bd->cancelarContacto($usuario);
				$respuesta = $this->mensajes['ayuda'];
				break;

			case '/contactar':
				$this->bd->solicitarContacto($usuario);
				$respuesta = $this->mensajes['contactar'];
				$teclado = json_encode($replyKeyboardHide);
				break;

			case '/cancelar':
				if ($this->bd->contactoSolicitado($usuario)) {
					$respuesta = $this->mensajes['cancelar'];
				}
				$this->bd->cancelarContacto($usuario);
				break;

			case '/start':
				$this->bd->inicializarUsuario($usuario);
				$respuesta = $this->mensajes['bienvenida'];
				break;

			case '/settings':
				$this->bd->cancelarContacto($usuario);
				$respuesta = $this->mensajes['configuraciones'];
				break;

			default:
				if ($this->bd->contactoSolicitado($usuario)) {
					if ($this->bd->guardarMensajeContacto($usuario, $mensaje)) {
						/* Configuro los parámetros para notificar sobre el 
						 * contacto realizado */
						$first_name = utf8_decode($usuario->first_name);
						$last_name = utf8_decode($usuario->last_name);
						$username = utf8_decode($usuario->username);

						$contacto =
							"Contacto de: $first_name $last_name (@$username)\n\n".
							"Mensaje: $mensaje";

						// Envío el mensaje de contacto al creador del bot
						$this->botsApi->sendMessage(4838085, $contacto);

						// Configuro la respuesta para el usuario
						$respuesta = $this->mensajes['contactar_gracias'];
					}
					else {
						$respuesta = $this->mensajes['contactar_error'];
						$teclado = json_encode($replyKeyboardHide);
					}
				}
				else {
					$respuesta = $this->mensajes['default'];
				}
		}

		// Envío la respuesta correspondiente al chat de origen
		$this->botsApi->sendMessage($usuario->id,
									$respuesta,
									'Markdown',
									$teclado);
	}
}
?>